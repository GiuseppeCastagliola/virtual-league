package com.edreamsodigeo;

import com.edreamsodigeo.model.Match;
import com.edreamsodigeo.model.Team;
import com.edreamsodigeo.repository.LeagueRepository;
import com.edreamsodigeo.repository.MatchRepository;
import com.edreamsodigeo.repository.TeamRepository;
import com.edreamsodigeo.web.v1.request.LeagueRequest;
import com.edreamsodigeo.web.v1.request.MatchResultRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CreateLeagueIntegrationTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(CreateLeagueIntegrationTest.class);
    public static final List<Team> TEAM_LIST = List.of(new Team("Rayo"),
            new Team("Atleti"),
            new Team("Villareal"),
            new Team("Getafe"),
            new Team("Valladolid"),
            new Team("Athletic"),
            new Team("Girona"),
            new Team("Betis"),
            new Team("Elche"));
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private LeagueRepository leagueRepository;
    @Autowired
    private MatchRepository matchRepository;

    @Test
    void createRoundRobinLeague() throws Exception {
        String leagueUuid = createLeague(TEAM_LIST, "ROUND_ROBIN");
        Assertions.assertTrue(leagueRepository.existsById(UUID.fromString(leagueUuid)));
        Assertions.assertEquals(teamRepository.findAll().size(), TEAM_LIST.size());
        List<Match> allMatches = matchRepository.findAll();
        int firstOp = TEAM_LIST.size() % 2 == 0 ? TEAM_LIST.size() - 1 : TEAM_LIST.size();
        int expectedMatches = firstOp * (TEAM_LIST.size() / 2);
        Assertions.assertEquals(expectedMatches, allMatches.size());
        allMatches.forEach(match -> LOGGER.info(match.toString()));
        updateRandomResults(leagueUuid, allMatches);
        LOGGER.info(retrieveLeagueRankings(leagueUuid));
        leagueRepository.deleteById(UUID.fromString(leagueUuid));
    }

    @Test
    void createTournamentLeague() throws Exception {
        String leagueUuid = createLeague(TEAM_LIST, "TOURNAMENT_LEAGUE");
        Assertions.assertTrue(leagueRepository.existsById(UUID.fromString(leagueUuid)));
        Assertions.assertEquals(teamRepository.findAll().size(), TEAM_LIST.size());
        List<Match> allMatches = matchRepository.findAll();
        Assertions.assertEquals(16, allMatches.size());
        allMatches.forEach(match -> LOGGER.info(match.toString()));
        updateRandomResults(leagueUuid, allMatches);
        LOGGER.info(retrieveLeagueRankings(leagueUuid));
        leagueRepository.deleteById(UUID.fromString(leagueUuid));
    }

    @Test
    void createTournamentLeagueKnockout() throws Exception {
        String leagueUuid = createLeague(TEAM_LIST, "TOURNAMENT_KNOCKOUT");
        Assertions.assertTrue(leagueRepository.existsById(UUID.fromString(leagueUuid)));
        Assertions.assertEquals(teamRepository.findAll().size(), TEAM_LIST.size());
        List<Match> allMatches = matchRepository.findAll();
        Assertions.assertEquals(TEAM_LIST.size() / 2, allMatches.size());
        updateRandomResults(leagueUuid, allMatches);
        List<Match> allMatchesUpdated = matchRepository.findAll();
        allMatchesUpdated.forEach(m -> LOGGER.info(m.toString()));
        leagueRepository.deleteById(UUID.fromString(leagueUuid));
        teamRepository.deleteAll();
    }

    private void updateRandomResults(String leagueUuid, List<Match> allMatches) throws Exception {
        for (Match match : allMatches) {
            UUID matchUUID = match.getId();
            MatchResultRequest matchResultRequest = new MatchResultRequest();
            Random random = new Random();
            matchResultRequest.setAwayScore(random.nextInt(5));
            matchResultRequest.setHomeScore(random.nextInt(5));
            String matchUpdated = mockMvc.perform(put("/virtual-league/v1/leagues/" + leagueUuid + "/matches/" + matchUUID)
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(matchResultRequest)))
                    .andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString();
            LOGGER.info(matchUpdated);
        }
    }

    private String createLeague(List<Team> teams, String leagueType) throws Exception {
        LeagueRequest leagueRequest = getLeagueRequest(teams, leagueType);
        String leagueUuid = mockMvc.perform(post("/virtual-league/v1/leagues")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(leagueRequest)))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        LOGGER.info("League UUID {}", leagueUuid);
        return leagueUuid;
    }

    private LeagueRequest getLeagueRequest(List<Team> teams, String leagueType) {
        LeagueRequest leagueRequest = new LeagueRequest();
        leagueRequest.setLeagueName("TestLeague");
        leagueRequest.setLeagueType(leagueType);
        leagueRequest.setTeams(teams);
        leagueRequest.setGroupNumber(2);
        return leagueRequest;
    }

    private String retrieveLeagueRankings(String leagueUuid) throws Exception {
        return mockMvc.perform(get("/virtual-league/v1/leagues/" + leagueUuid + "/rankings")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
    }

}