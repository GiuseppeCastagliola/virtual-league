package com.edreamsodigeo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.StringJoiner;
import java.util.UUID;

@Entity
@Table(name = Match.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Match {
    protected static final String TABLE_NAME = "MATCHES";
    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;
    @ManyToOne(cascade = CascadeType.ALL)
    private Team homeTeam;
    @ManyToOne(cascade = CascadeType.ALL)
    private Team awayTeam;
    private int homeScore;
    private int awayScore;
    private int groupId;

    private Match(Builder builder) {
        id = builder.id;
        homeTeam = builder.homeTeam;
        awayTeam = builder.awayTeam;
        groupId = builder.groupId;
        homeScore = builder.homeScore;
        awayScore = builder.awayScore;
    }

    protected Match() {

    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public int getGroupId() {
        return groupId;
    }

    public UUID getId() {
        return id;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builderCopyOf(Match match) {
        return new Builder().id(match.getId())
                .homeTeam(match.getHomeTeam())
                .awayTeam(match.getAwayTeam())
                .homeScore(match.getHomeScore())
                .awayScore(match.getAwayScore())
                .groupId(match.getGroupId());
    }

    public static final class Builder {
        private UUID id;
        private Team homeTeam;
        private Team awayTeam;
        private int groupId;
        private int homeScore;
        private int awayScore;

        private Builder() {
        }

        public Builder homeTeam(Team team) {
            homeTeam = team;
            return this;
        }

        public Builder awayTeam(Team team) {
            awayTeam = team;
            return this;
        }

        public Builder groupId(int groupId) {
            this.groupId = groupId;
            return this;
        }

        public Builder homeScore(int score) {
            this.homeScore = score;
            return this;
        }

        public Builder awayScore(int score) {
            this.awayScore = score;
            return this;
        }

        public Builder id(UUID id) {
            this.id = id;
            return this;
        }

        public Match build() {
            return new Match(this);
        }

    }

    @Override
    public String toString() {
        return new StringJoiner(" ", Match.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add(homeTeam.getName())
                .add(awayTeam.getName())
                .add(String.valueOf(homeScore))
                .add(String.valueOf(awayScore))
                .add("groupId=" + groupId)
                .toString();
    }
}
