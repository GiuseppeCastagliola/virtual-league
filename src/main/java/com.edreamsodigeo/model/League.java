package com.edreamsodigeo.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.UUID;

@Entity
@Table(name = League.TABLE_NAME)
public class League {
    public static final String TABLE_NAME = "LEAGUES";
    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;
    @OneToMany
    private Collection<Team> leagueTeams;
    @OneToMany(cascade = CascadeType.ALL)
    private Collection<Match> season;
    private String name;
    private String type;

    protected League() {
    }

    public UUID getId() {
        return id;
    }

    public Collection<Team> getLeagueSquads() {
        return leagueTeams;
    }

    public Collection<Match> getSeason() {
        return season;
    }

    public String getName() {
        return name;
    }

    private League(UUID id, Collection<Team> teams, Collection<Match> season, String name, String type) {
        this.id = id;
        this.leagueTeams = teams;
        this.season = season;
        this.name = name;
        this.type = type;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private UUID id;
        private Collection<Team> leagueTeams;
        private Collection<Match> season;
        private String name;
        private String type;

        private Builder() {
        }

        public Builder leagueTeams(Collection<Team> teams) {
            leagueTeams = teams;
            return this;
        }

        public Builder season(Collection<Match> season) {
            this.season = season;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public League build() {
            return new League(id, leagueTeams, season, name, type);
        }
    }
}

