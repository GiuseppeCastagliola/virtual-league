package com.edreamsodigeo.model;

import java.util.StringJoiner;

public class LeagueRanking implements Comparable<LeagueRanking> {
    private String teamName;
    private int points;
    private int scored;
    private int conceded;

    public LeagueRanking(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getScored() {
        return scored;
    }

    public void setScored(int scored) {
        this.scored = scored;
    }

    public int getConceded() {
        return conceded;
    }

    public void setConceded(int conceded) {
        this.conceded = conceded;
    }

    @Override
    public int compareTo(LeagueRanking other) {
        int pointDiff = Integer.compare(this.points, other.points);
        return pointDiff != 0 ? pointDiff : Integer.compare(this.scored - this.conceded, other.scored - other.conceded);
    }

    @Override
    public String toString() {
        return new StringJoiner(" | ")
                .add(teamName)
                .add(String.valueOf(points))
                .add(String.valueOf(scored))
                .add(String.valueOf(conceded))
                .toString();
    }
}
