package com.edreamsodigeo.web.v1;

import com.edreamsodigeo.model.League;
import com.edreamsodigeo.model.LeagueRanking;
import com.edreamsodigeo.model.Match;
import com.edreamsodigeo.service.LeagueService;
import com.edreamsodigeo.web.APIRestConstants;
import com.edreamsodigeo.web.v1.mapper.LeagueMapper;
import com.edreamsodigeo.web.v1.request.LeagueRequest;
import com.edreamsodigeo.web.v1.request.LeagueResponse;
import com.edreamsodigeo.web.v1.request.MatchResultRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = APIRestConstants.PATH_V1_VERSION,
        produces = APIRestConstants.JSON_CONTENT_TYPE,
        consumes = APIRestConstants.JSON_CONTENT_TYPE)
public class VirtualLeagueResource {

    private final LeagueMapper mapper;

    private final LeagueService leagueService;

    @Autowired
    public VirtualLeagueResource(LeagueMapper mapper, LeagueService leagueService) {
        this.mapper = mapper;
        this.leagueService = leagueService;
    }

    @GetMapping(value = "/ping")
    public String ping() {
        return APIRestConstants.PONG_MESSAGE;
    }

    @GetMapping(value = "/leagues/{uuid}")
    public LeagueResponse getLeague(@PathVariable("uuid") String uuid) {
        return mapper.toLeagueResponse(leagueService.getLeague(uuid));
    }

    @PostMapping(value = "/leagues")
    public String createLeague(@RequestBody LeagueRequest leagueRequest) {
        return leagueService.create(leagueRequest).getId().toString();
    }

    @PatchMapping(value = "/leagues/{uuid}")
    public League updateLeague(@PathVariable("uuid") String uuid, League league) {
        return leagueService.update(uuid, league);
    }

    @DeleteMapping(value = "/leagues/{uuid}")
    public void deleteLeague(@PathVariable("uuid") String uuid) {
        leagueService.delete(uuid);
    }

    @PutMapping(value = "/leagues/{uuid}/matches/{matchUuid}")
    public Match updateMatches(@PathVariable("uuid") String uuid,
                               @PathVariable("matchUuid") String matchUuid,
                               @RequestBody MatchResultRequest matchResultRequest) {
        return leagueService.saveScore(UUID.fromString(matchUuid), matchResultRequest);
    }

    @GetMapping(value = "/leagues/{uuid}/rankings")
    public Map<Integer, List<LeagueRanking>> getLeagueRanking(@PathVariable("uuid") String uuid) {
        return leagueService.getRanking(UUID.fromString(uuid));
    }
}
