package com.edreamsodigeo.web.v1.request;

import java.io.Serializable;

public class MatchResultRequest implements Serializable {
    private int homeScore;
    private int awayScore;

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }
}
