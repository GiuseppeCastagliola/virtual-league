package com.edreamsodigeo.web.v1.request;

import com.edreamsodigeo.model.Match;
import com.edreamsodigeo.model.Team;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Collection;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeagueResponse {
    private UUID id;
    private Collection<Team> leagueTeams;
    private Collection<Match> season;
    private String name;
    private String type;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Collection<Team> getLeagueSquads() {
        return leagueTeams;
    }

    public void setLeagueSquads(Collection<Team> leagueTeams) {
        this.leagueTeams = leagueTeams;
    }

    public Collection<Match> getSeason() {
        return season;
    }

    public void setSeason(Collection<Match> season) {
        this.season = season;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
