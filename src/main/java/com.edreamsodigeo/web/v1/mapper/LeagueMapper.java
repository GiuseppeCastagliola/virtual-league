package com.edreamsodigeo.web.v1.mapper;

import com.edreamsodigeo.model.League;
import com.edreamsodigeo.web.v1.request.LeagueRequest;
import com.edreamsodigeo.web.v1.request.LeagueResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LeagueMapper {
    League fromLeagueRequest(LeagueRequest leagueRequest);

    LeagueResponse toLeagueResponse(League league);
}
