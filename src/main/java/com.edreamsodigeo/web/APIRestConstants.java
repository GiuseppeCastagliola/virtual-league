package com.edreamsodigeo.web;

public class APIRestConstants {

  public static final String JSON_CONTENT_TYPE = "application/json;charset=UTF-8";
  public static final String PATH_V1_VERSION = "/virtual-league/v1";
  public static final String PONG_MESSAGE = "Challenge v1 is up and running";

}
