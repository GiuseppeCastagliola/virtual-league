package com.edreamsodigeo.repository;

import com.edreamsodigeo.model.League;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LeagueRepository extends JpaRepository<League, UUID> {

}
