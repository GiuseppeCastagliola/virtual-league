package com.edreamsodigeo.repository;

import com.edreamsodigeo.model.League;
import com.edreamsodigeo.model.Match;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MatchRepository extends JpaRepository<Match, UUID> {

}
