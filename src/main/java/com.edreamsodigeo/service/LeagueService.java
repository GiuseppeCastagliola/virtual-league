package com.edreamsodigeo.service;

import com.edreamsodigeo.model.League;
import com.edreamsodigeo.model.LeagueRanking;
import com.edreamsodigeo.model.Match;
import com.edreamsodigeo.web.v1.request.LeagueRequest;
import com.edreamsodigeo.web.v1.request.MatchResultRequest;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface LeagueService {

    League getLeague(String id);

    League create(LeagueRequest leagueRequest);

    League update(String id, League league);

    void delete(String id);

    Match saveScore(UUID id, MatchResultRequest resultRequest);

    Map<Integer, List<LeagueRanking>> getRanking(UUID leagueId);
}
