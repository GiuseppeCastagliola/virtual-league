package com.edreamsodigeo.service;

import com.edreamsodigeo.model.League;
import com.edreamsodigeo.model.LeagueRanking;
import com.edreamsodigeo.model.Match;
import com.edreamsodigeo.model.Team;
import com.edreamsodigeo.repository.LeagueRepository;
import com.edreamsodigeo.repository.MatchRepository;
import com.edreamsodigeo.repository.TeamRepository;
import com.edreamsodigeo.web.v1.request.LeagueRequest;
import com.edreamsodigeo.web.v1.request.MatchResultRequest;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class LeagueServiceImpl implements LeagueService {

    private static final int DEFAULT_GROUP = 0;
    private final LeagueRepository leagueRepository;
    private final MatchRepository matchRepository;
    private final TeamRepository teamRepository;

    @Autowired
    public LeagueServiceImpl(LeagueRepository leagueRepository,
                             MatchRepository matchRepository,
                             TeamRepository teamRepository) {
        this.leagueRepository = leagueRepository;
        this.matchRepository = matchRepository;
        this.teamRepository = teamRepository;
    }

    @Override
    public League getLeague(String id) {
        return leagueRepository.getById(UUID.fromString(id));
    }

    @Override
    public League create(LeagueRequest leagueRequest) {
        List<Match> matches;
        List<Team> teams = leagueRequest.getTeams()
                .stream()
                .map(teamRepository::save)
                .collect(Collectors.toList());
       switch (leagueRequest.getLeagueType()) {
            case "ROUND_ROBIN":
                matches = generateRoundRobinMatches(teams, DEFAULT_GROUP);
                break;
            case "ROUND_ROBIN_HOME_AWAY":
                matches = generateRoundRobinHomeAwayMatches(teams);
                break;
            case "TOURNAMENT_LEAGUE":
                matches = generateTournamentLeagueMatches(teams, leagueRequest.getGroupNumber());
                break;
            case "TOURNAMENT_KNOCKOUT":
                matches = generateKnockoutMatches(teams);
                break;
            default:
                matches = Collections.emptyList();
        }

        return saveLeague(teams, matches);
    }

    private List<Match> generateKnockoutMatches(List<Team> teams) {
        List<Match> m = new ArrayList<>();
        List<Team> teams2Match = new ArrayList<>(teams);
        while (teams2Match.size() > 1) {
            m.add(saveMatch(teams.get(0), teams.get(1), DEFAULT_GROUP));
            teams2Match = teams2Match.subList(2, teams2Match.size());
        }
        return m;
    }

    private List<Match> generateRoundRobinHomeAwayMatches(List<Team> teams) {
        throw new UnsupportedOperationException();
    }

    private List<Match> generateRoundRobinMatches(List<Team> teams, int groupId) {
        var indexes = IntStream.range(0, teams.size())
                .boxed().collect(Collectors.toSet());
        return Sets.combinations(indexes, 2)
                .stream()
                .map(Set::iterator)
                .map(it -> saveMatch(teams.get(it.next()), teams.get(it.next()), groupId))
                .collect(Collectors.toList());
    }

    private List<Match> generateTournamentLeagueMatches(List<Team> teams, int numberOfGroups) {
        Map<Integer, List<Team>> groupTeams = new HashMap<>();
        for (int i = 0; i < teams.size(); i++) {
            int groupNumber = i % numberOfGroups;
            if (Objects.isNull(groupTeams.get(groupNumber))) {
                groupTeams.put(groupNumber, new ArrayList<>());
            }
            groupTeams.get(groupNumber).add(teams.get(i));
        }
        List<Match> matches = new ArrayList<>();
        for (Integer group : groupTeams.keySet()) {
            matches.addAll(generateRoundRobinMatches(groupTeams.get(group), group));
        }
        return matches;
    }

    private League saveLeague(List<Team> teams, List<Match> matches) {
        return leagueRepository.save(League.builder()
                .leagueTeams(teams)
                .season(matches)
                .build());
    }

    private Match saveMatch(Team home, Team away, int groupId) {
        return matchRepository.save(Match.builder()
                .homeTeam(home)
                .awayTeam(away)
                .groupId(groupId)
                .build());
    }

    @Override
    public League update(String id, League league) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(String id) {
        leagueRepository.delete(leagueRepository.getById(UUID.fromString(id)));
    }

    @Override
    public Match saveScore(UUID id, MatchResultRequest resultRequest) {
        Match match = matchRepository.getById(id);
        return matchRepository.save(Match.builderCopyOf(match)
                .awayScore(resultRequest.getAwayScore())
                .homeScore(resultRequest.getHomeScore())
                .build());
    }

    @Override
    public Map<Integer, List<LeagueRanking>> getRanking(UUID leagueId) {
        League league = leagueRepository.getById(leagueId);
        Map<Integer, Map<UUID, LeagueRanking>> rankingsTeamsByGroup = new HashMap<>();
        for (Match match : league.getSeason()) {
            Team homeTeam = match.getHomeTeam();
            Team awayTeam = match.getAwayTeam();
            rankingsTeamsByGroup.putIfAbsent(match.getGroupId(), new HashMap<>());
            rankingsTeamsByGroup.get(match.getGroupId()).putIfAbsent(homeTeam.getId(), new LeagueRanking(homeTeam.getName()));
            rankingsTeamsByGroup.get(match.getGroupId()).putIfAbsent(awayTeam.getId(), new LeagueRanking(awayTeam.getName()));

            LeagueRanking leagueRankingHomeTeam = rankingsTeamsByGroup.get(match.getGroupId()).get(homeTeam.getId());
            LeagueRanking leagueRankingAwayTeam = rankingsTeamsByGroup.get(match.getGroupId()).get(awayTeam.getId());

            leagueRankingHomeTeam.setScored(leagueRankingHomeTeam.getScored() + match.getHomeScore());
            leagueRankingHomeTeam.setConceded(leagueRankingHomeTeam.getConceded() + match.getAwayScore());
            leagueRankingAwayTeam.setScored(leagueRankingAwayTeam.getScored() + match.getAwayScore());
            leagueRankingAwayTeam.setConceded(leagueRankingAwayTeam.getConceded() + match.getHomeScore());
            if (match.getHomeScore() == match.getAwayScore()) {
                leagueRankingHomeTeam.setPoints(leagueRankingHomeTeam.getPoints() + 1);
                leagueRankingAwayTeam.setPoints(leagueRankingAwayTeam.getPoints() + 1);
            } else {
                if (match.getHomeScore() > match.getAwayScore()) {
                    leagueRankingHomeTeam.setPoints(leagueRankingHomeTeam.getPoints() + 3);
                } else {
                    leagueRankingAwayTeam.setPoints(leagueRankingAwayTeam.getPoints() + 3);
                }
            }

        }
        Map<Integer, List<LeagueRanking>> ranking = new HashMap<>();
        rankingsTeamsByGroup.forEach(
                (groupId, rankingGroup) -> {
                    List<LeagueRanking> groupRanking = new ArrayList<>(rankingGroup.values());
                    groupRanking.sort(Comparator.reverseOrder());
                    ranking.put(groupId, groupRanking);
                }
        );
        return ranking;


    }
}
