# Virtual League #
* [League definition](https://en.wiktionary.org/wiki/league)

A virtual league can be used to organize and manage a competition between different teams/participants. It could be a video game league, basketball league a soccer league and so on.
## Relationships

A **League** is composed by several **Teams**.

A **Team** faces another **Team** in a **Match**

Depending on the **Matches** played the **League** has a **LeagueRanking** that represents the position of every **Team**